ARD := ~/proj/sdk/arduino-1.6.5/arduino
#OTAUPLOAD := ~/.arduino15/packages/esp8266/hardware/esp8266/1.6.5-1044-g170995a/tools/espota.py
OTAUPLOAD := ~/.arduino15/packages/esp8266/hardware/esp8266/1.6.5-1144-gcef895b/tools/espota.py

BOARD	:= esp8266com:esp8266:nodemcuv2
IP	:= roomba

%.html.h: %.html
	reswrap -ta -o $@ $<

%.ico.h: %.ico
	reswrap -ta -o $@ $<

all: webRoot.html.h webConfig.html.h webUpgrade.html.h favicon.ico.h
	$(ARD)   --board $(BOARD)		\
		 --pref build.path=build	\
		--preferences-file ./preferences.txt \
	         --verbose --pref build.path=build --verify Roomba.ino

upload:
	curl -F "image=@build/Roomba.cpp.bin" $(IP)/_upgrade

flash:
	$(ARD) --preferences-file ./preferences.txt --board $(BOARD) \
		--verbose --pref build.path=build --port /dev/ttyUSB0 --upload Roomba.ino
clean:
	rm -rf build
