#include <Esp.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <WiFiManager.h>

#include <EEPROM.h>

#include "Framework.h"

extern "C" {
#include <user_interface.h>
}

#include "app.h"

#define WEB_LISTEN_PORT 80

WiFiManager wifiManager;
MDNSResponder mdns;

Framework::Framework(const char* appName) :
  Debug(Serial), webServer(WEB_LISTEN_PORT)
{
  this->appName = appName;
}

void Framework::begin()
{
  Serial.begin(115200);
  Serial.setDebugOutput(true);

  Serial.println("");
  EEPROM.begin(sizeof(prefs));
  EEPROM.get(0, prefs);
  EEPROM.end();
  if(prefs.version != PREFS_VERSION) {
    Debug.println("prefs uninitialized, setting defaults");
    prefs.version = PREFS_VERSION;
    strcpy(prefs.ssid, "frmanet2");
    strcpy(prefs.passwd, "fm6789012345");
    savePrefs();
  }

  if(!wifiManager.autoConnect("Ropomba AP")) {
      Serial.println("Failed, restarting");
      ESP.restart();
  }

  // Connect WiFi
  #if 0
  wifi_set_phy_mode(PHY_MODE_11B);
  IPAddress localIP;
  Debug.print("Connecting to ");
  Debug.print(prefs.ssid);
  Debug.print(":");
  Debug.println(prefs.passwd);
  WiFi.mode(WIFI_STA);

  int retries = 60;
  WiFi.begin(prefs.ssid, prefs.passwd);
  while (retries-- > 0 && WiFi.status() != WL_CONNECTED) {
    delay(500);
    Debug.print(retries);
    Debug.print(".");
  }
  Debug.println("");
  if(WiFi.status() == WL_CONNECTED) {
    Debug.println("WiFi connected");
    Debug.print("  IP address: ");
    Debug.println(WiFi.localIP());
    wifi_set_sleep_type(LIGHT_SLEEP_T);
    localIP = WiFi.localIP();
  }
  else {
    IPAddress apIP(192, 168, 1, 1);
    WiFi.mode(WIFI_AP);
    WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
    WiFi.softAP(appName, "gurgel42");
    localIP = WiFi.softAPIP();
    Debug.print("Failed to connect, starting in AP mode.");
    Debug.printf("Starting AP with ssid:%s and password:%s\n\r", appName, "gurgel42");
    Debug.print("Connect to wifi and point browser to http://");
    Debug.print(localIP);
    Debug.println("/config");
  }
  #endif

  if (mdns.begin(appName, WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }
  // Start web server
  webServer.begin();
  webServer.on("/restart", [this]() {
    webServer.send(200, "text/plain", "OK");
    ESP.restart();
  });
  webServer.on("/setwifi", [this]() {
      String ssid = webServer.arg("ssid");
      String pwd = webServer.arg("passwd");
      if(ssid != 0 && pwd != 0) {
        Debug.print("Set wifi to ssid:");
        Debug.print(ssid);
        Debug.print(" pwd:");
        Debug.println(pwd);
        strcpy(prefs.ssid, ssid.c_str());
        strcpy(prefs.passwd, pwd.c_str());
        savePrefs();
        webServer.send(200, "text/plain", "OK");
      }
      webServer.send(501, "text/plain", "Wrong parameters");
  });
  fileServer.setup(&webServer);
  httpUpdater.setup(&webServer);
  Debug.println("Web server started");
  ntp.begin();
}


void Framework::run()
{
  webServer.handleClient();
  ntp.run();
#if 0
  MDNS.update();
#endif
}

void Framework::savePrefs()
{
        EEPROM.begin(sizeof(prefs));
        EEPROM.put(0, prefs);
        EEPROM.end();
}
