#ifndef _SLACK_H_
#define _SLACK_H_

#include <WiFiClientSecure.h>

class Slack {
	const char* fingerprint = "AB F0 5B A9 1A E0 AE 5F CE 32 2E 7C 66 67 49 EC DD 6D 6A 38";
public:
	void begin() {}
	void run() {}
	//
	// token   - Get it here: https://api.slack.com/web.
	//		Example: xoxp-00000000000-11111111111-22222222222-3333333333
	// from    - Sender name user (but actually sent from slackbot)
	//		Example: Roomba			  
	// channel - The slack channel name or @your-name
	//		Example: @frma71
	//		Example: general
	//
	void send(const char* token, const char* from, const char* channel, String msg)
	{
		String url;

		url = (url + "/api/chat.postMessage?token=") + token;
		url = (url + "&channel=") + channel;
		url = (url + "&text=") + msg;
		url = (url + "&username=") + from;

		url.replace(" ", "%20");
		url.replace("@", "%40");
		url.replace(":", "%3A");

		WiFiClientSecure client;

		if (client.connect("slack.com", 443)) {
			Serial.println("connected");
			if(client.verify(fingerprint, "slack.com")) {
				Serial.println("Certificate does match");
				String str = "GET " + url + " HTTP/1.1\r\nHost: slack.com\r\nConnection: close\r\n\r\n";
				client.print(str);
				Serial.println(str);
			}
			else {
				Serial.println("Certificate does not match");
			}
			client.stop();
		} else {
			// if you didn't get a connection to the server:
			Serial.println("connection failed");
		}
	}
};

#endif /* _SLACK_H_ */
