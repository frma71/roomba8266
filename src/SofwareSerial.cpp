#include <stdlib.h>
#include <Arduino.h>
#include <stdint.h>
#include "SoftwareSerial.h"

static inline u8 chbit(u8 data, u8 bit)
{
  if ((data & bit) != 0)
    return 1;
  else
    return 0;
}

SoftwareSerial::SoftwareSerial(int baud, int txPin) {
  this->txPin = txPin;
  this->bitTime = (scale*tickPerUS*1000000) / baud;
}
void SoftwareSerial::begin() {
  digitalWrite(txPin, 1);
  pinMode(txPin, OUTPUT);
}

size_t SoftwareSerial::write(uint8_t data)
{
  unsigned i;
  unsigned start;
  int bit = 1;
 
  unsigned int d = (unsigned int)data;
  d <<= 2;    // Start bit (and an extra one to get the timing right)
  d |= 0x401; // Stop bit
  os_intr_lock();
  start = now();
  for(i = 0; i <= 10; i++) {
    digitalWrite(txPin, d&1);
    d >>= 1;
    wait_until(start, bit++);
  }
  os_intr_unlock();

  // Delay after byte, for new sync
  delayMicroseconds((bitTime*6)/scale/tickPerUS);
  return 1;
}
