#include <EEPROM.h>
#include <Esp.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <WiFiClient.h>
#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <WiFiUdp.h>

#include "RoombaCompileTimeConfig.h"

#include "Framework.h"
#include "app.h"
#include "SoftwareSerial.h"
#include "Slack.h"

static const char* name = "roomba";
static const int id = 1;
static const int version = 3;

static const int roombaBaud = 115200;
static const int roombaTxPin = 12;
static const int wakeupPin = 13;


Framework fw(name);
Stream& Debug = fw.Debug;


/* Use input from one Stream and output to another.
 * This is done since we want to use software(bitbanged) TX and
 * hardware RX
 */
class CombinedSerial : public Stream {
public:
   CombinedSerial(Stream& ina, Stream& outa) :
  in(ina),out(outa)
  {}
#if 0
t wre() {
#endif
  size_t write(uint8_t c) { return out.write(c); };
  int available()         { return in.available();}
  int read()              { return in.read();}
  int peek()              { return in.peek();}
  void flush() {}
private:
  Stream& in;
  Stream& out;
};
SoftwareSerial swserial(roombaBaud, roombaTxPin);
CombinedSerial roomba(Serial, swserial);

#define WAIT ((unsigned char)-1)


class JsonGen {
public:
  JsonGen(int bufSize)
  {
    buff = new char[bufSize];
    buffSize = bufSize;
}
void start() {
    len = 0;
    buff[0] = 0;
    append("{");
    first = 1;
}
void end() {
    append("}");
}
void write(Stream& s) {
    while(len & 3) // Potentiall workaround for webserver unaligned access problem
      append(" ");
  s.write((const char*)buff, len);
}
void push(const char* name) {
    if(!first) {
      append(",");
  }
  first = 1;
  append("\"");
  append(name);
  append("\":{");
}
void pop() {
    append("}");
}
void elem(const char* name, const char* val) {
    if(!first)
      append(",");
  first = 0;
  append("\"");
  append(name);
  append("\":\"");
  append(val);
  append("\"");
}
void elem(const char* name, int val) {
    char buff[16];
    itoa(val, buff, 10);
    elem(name, buff);
}
int length() {
    return strlen(buff);
}
private:
  void append(const char* str) {
    int l = strlen(str);
    if(len + l < buffSize) {
      strcpy(&buff[len], (char*)str);
      len += l;
  } else {
      Debug.println("JSON buffer overflow");
  }
}
int first;
int buffSize;
char* buff;
int len;
};

struct SensorData {
  boolean valid;
  struct {
    unsigned char caster;
    unsigned char left;
    unsigned char right;
} wheeldrop;
struct {
    unsigned char left;
    unsigned char right;
} bumper;
unsigned char wall;
struct {
    unsigned char left;
    unsigned char fleft;
    unsigned char fright;
    unsigned char right;
} cliff;
unsigned char virtualwall;
struct {
    unsigned char left;
    unsigned char right;
    unsigned char brush;
    unsigned char vacuum;
    unsigned char side;
} overcurrent;
struct {
    unsigned char left;
    unsigned char right;
} dirt;
unsigned char remotecmd;
struct {
    unsigned char power:1;
    unsigned char spot:1;
    unsigned char clean:1;
    unsigned char max:1;
} buttons;
signed short  distance;
signed short  angle;
unsigned char charging;
unsigned short voltage;
signed short   current;
signed char    temp;
unsigned short charge;
unsigned short capacity;
};


class MyApp {
public:
  MyApp() :
  json(1024),
  rawDataSock(4711)
  {}
private:
  JsonGen json;
  Notifier notifier;
  WiFiServer rawDataSock;
  struct Cmds {
    const char* cmd;
    int len;
    const unsigned char codes[3];
    void (MyApp::*f)(void);
};

void wakeup() {
  if(!getSensorData()) {
    pinMode(wakeupPin, OUTPUT);
    digitalWrite(wakeupPin, 0);
    delay(500);
    digitalWrite(wakeupPin, 1);
    delay(100);
    getSensorData();
}
}
void sendSeq(const unsigned char* codes, int len) {
  Debug.print("Send ");
  Debug.println(len);
  for(int i = 0; i < len; i++) {
    if(codes[i] == WAIT)
      delay(1000);
  else {
      Debug.print("W: ");
      Debug.println((int)codes[i]);
      roomba.write((uint8_t)codes[i]);
  }
}

}
void handleSend(void) {
    int pos = 0;
    int val;
    int n = 0;

    wakeup();

    String data = fw.webServer.arg(1);

    val = data.toInt();
    roomba.write(val);
    n++;
    pos=data.indexOf(',',pos);
    while(pos >= 0) {
      data = data.substring(pos+1);
      val = (uint8_t)data.toInt();
      if(val == WAIT)
        delay(1000);
    else
        roomba.write(val);
    n++;
    pos=data.indexOf(',',pos);
}
fw.webServer.send(200, "text/plain", "OK");
}
inline int readb(const char *ptr, int bit) {
    return (int)((ptr[0] >> (bit)) & 1);
}
inline int readu8(const char *ptr) {
    return (int)((unsigned int)ptr[0]);
}
inline int reads8(const char *ptr) {
    return (int)(signed char)ptr[0];
}
inline unsigned short readu16(const char *ptr) {
    return ((((unsigned short)ptr[0])<<8) + (unsigned short)ptr[1]);
}
inline signed short reads16(const char *ptr) {
    return (signed short)readu16(ptr);
}
SensorData sensorData;
int getSensorData() {
    int idx = 0;
    long start = millis();
    static char buff[26]; // FIXME: Maybe we can have this on the stack now

    sensorData.valid = false;

    Debug.println("getSensorData");
    static const unsigned char cmd[] = { 128, 142, 0};
    sendSeq(cmd, 3);
    while(idx < 26) {
      if(Serial.available()) {
          buff[idx++] = Serial.read();
      }
      else if(millis() - start > 1000) {
        Debug.println("Read timed out");
        return 0;
    }
}
sensorData.wheeldrop.caster = readb(&buff[0],4);
sensorData.wheeldrop.left   = readb(&buff[0],3);
sensorData.wheeldrop.right  = readb(&buff[0],2);
sensorData.bumper.left      = readb(&buff[0],1);
sensorData.bumper.right     = readb(&buff[0],0);
sensorData.wall             = readu8(&buff[1]);
sensorData.cliff.left       = readu8(&buff[2]);
sensorData.cliff.fleft      = readu8(&buff[3]);
sensorData.cliff.fright     = readu8(&buff[4]);
sensorData.cliff.right      = readu8(&buff[5]);
sensorData.virtualwall      = readu8(&buff[6]);
sensorData.overcurrent.left = readb(&buff[7],4);
sensorData.overcurrent.right  = readb(&buff[7],3);
sensorData.overcurrent.brush  = readb(&buff[7],2);
sensorData.overcurrent.vacuum = readb(&buff[7],1);
sensorData.overcurrent.side   = readb(&buff[7],0);
sensorData.dirt.left        = readu8(&buff[8]);
sensorData.dirt.right       = readu8(&buff[9]);
sensorData.remotecmd        = readu8(&buff[10]);
sensorData.buttons.power    = readb(&buff[11],3);
sensorData.buttons.spot     = readb(&buff[11],2);
sensorData.buttons.clean    = readb(&buff[11],1);
sensorData.buttons.max      = readb(&buff[11],0);
sensorData.distance         = reads16(&buff[12]);
sensorData.angle          = reads16(&buff[14]);
sensorData.charging       = readu8(&buff[16]);
sensorData.voltage        = readu16(&buff[17]);
sensorData.current        = reads16(&buff[19]);
sensorData.temp           = reads8(&buff[21]);
sensorData.charge         = readu16(&buff[22]);
sensorData.capacity       = readu16(&buff[24]);
sensorData.valid = true;


return 1;
}
void handleSensors(void) {
    Debug.println("handleSensors");
    wakeup();
    if(sensorData.valid) {
      json.start();
      json.elem("version", version);
      json.elem("time", fw.getEpoch());
      json.elem("uptime", millis());
      json.elem("rssi",  WiFi.RSSI());
      json.push("wheeldrop");
      json.elem("caster", sensorData.wheeldrop.caster);
      json.elem("left",   sensorData.wheeldrop.left);
      json.elem("right",  sensorData.wheeldrop.right);
      json.pop();
      json.push("bump");
      json.elem("left",   sensorData.bumper.left);
      json.elem("right",  sensorData.bumper.right);
      json.pop();
      json.elem("wall",   sensorData.wall);
      json.push("cliff");
      json.elem("left",   sensorData.cliff.left);
      json.elem("fleft",  sensorData.cliff.fleft);
      json.elem("fright", sensorData.cliff.fright);
      json.elem("right",  sensorData.cliff.right);
      json.pop();
      json.elem("virtualwall", sensorData.virtualwall);
      json.push("overcurrent");
      json.elem("left",   sensorData.overcurrent.left);
      json.elem("right",  sensorData.overcurrent.right);
      json.elem("brush",  sensorData.overcurrent.brush);
      json.elem("vacuum", sensorData.overcurrent.vacuum);
      json.elem("side",   sensorData.overcurrent.side);
      json.pop();
      json.push("dirt");
      json.elem("left",   sensorData.dirt.left);
      json.elem("right",  sensorData.dirt.right);
      json.pop();
      json.elem("remotecmd", sensorData.remotecmd);
      json.push("buttons");
      json.elem("power",  sensorData.buttons.power);
      json.elem("spot",   sensorData.buttons.spot);
      json.elem("clean",  sensorData.buttons.clean);
      json.elem("max",    sensorData.buttons.max);
      json.pop();
      json.elem("distance", sensorData.distance);
      json.elem("angle",    sensorData.angle);
      json.elem("charging", sensorData.charging);
      json.elem("voltage",  sensorData.voltage);
      json.elem("current",  sensorData.current);
      json.elem("temp",     sensorData.temp);
      json.elem("charge",   sensorData.charge);
      json.elem("capacity", sensorData.capacity);
      json.end();
      fw.webServer.setContentLength(json.length());
      fw.webServer.send(200, "application/json", "");
      WiFiClient client = fw.webServer.client();
      json.write(client);
  }
  else {
      Debug.println("TIMEOUT");
      fw.webServer.send(501, "text/plain", "TIMEOUT ");
  }
}
void handleCmd() {
    int rv = 200;
    String rvMsg = "OK";
    String cmd = fw.webServer.arg(0);
    Debug.print("Got command: ");
    Debug.println(cmd);

    for(int i = 0; i < sizeof(cmds)/sizeof(cmds[0]); i++) {
      if(cmds[i].cmd == NULL) {
        rvMsg = "Command " + cmd + " not found";
        rv = 501;
        break;
    }
    if(cmds[i].len != 0)
      wakeup();

    if(cmd.equals(cmds[i].cmd)) {
        if(cmds[i].f) {
          (this->*cmds[i].f)();
          return;
      }
      else {
          sendSeq(cmds[i].codes, cmds[i].len);
      }
      break;
  }
}
fw.webServer.send(rv, "text/plain", rvMsg);
yield();
}
void handleStats(void) {
    Debug.println("handleStats");
    json.start();
    json.elem("version", version);
    json.elem("time", fw.getEpoch());
    json.elem("uptime", millis());
    json.elem("heap", ESP.getFreeHeap());
    json.elem("polls", pollCount);
    json.elem("lastClean", lastCleanTime);
    json.elem("lastDocked", lastDockedTime);
    json.elem("lastStall", lastStallTime);
    json.end();
    fw.webServer.setContentLength(json.length());
    fw.webServer.send(200, "application/json", "");
    WiFiClient client = fw.webServer.client();
    json.write(client);
}
void handleTestMsg() {
    notify("Test message from roomba: " + fw.webServer.arg(1));
    fw.webServer.send(200, "text/plain", "OK");
}
void handleNotFound(){
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += fw.webServer.uri();
    message += "\nMethod: ";
    message += (fw.webServer.method() == HTTP_GET)?"GET":"POST";
    message += "\nArguments: ";
    message += fw.webServer.args();
    message += "\n";
    for (uint8_t i=0; i<fw.webServer.args(); i++){
      message += " " + fw.webServer.argName(i) + ": " + fw.webServer.arg(i);
  }
  fw.webServer.send(404, "text/plain", message);
}
public:
  void begin() {
    swserial.begin();

    Debug.println();
    Debug.print("Welcome to arduino for esp8266/ESP-12 -");
    Debug.println(version);
    Debug.print("Device: ");
    Debug.print(name);
    Debug.print(":");
    Debug.println(id);

    WiFi.printDiag(Serial);

    fw.webServer.on("/cmd", [this]() { handleCmd();});

    digitalWrite(wakeupPin, 1);
    pinMode(wakeupPin, OUTPUT);

    notifier.begin();
    notify("Roomba booted");
}
void notify(String msg) {
//    notifier.send(Config::slackToken, Config::slackSender, Config::slackChannel, msg);
}
long lastPoll = 0;
long pollCount = 0;
unsigned int lastDockedTime;
unsigned int lastCleanTime;
unsigned int lastStallTime;

enum State {
    DOCKED,
    CLEANING,
    STALLED
} state;
void doPoll() {
    getSensorData();
    boolean docked = sensorData.cliff.left && sensorData.cliff.fleft && sensorData.cliff.right && sensorData.cliff.fright;
    boolean moving = (sensorData.distance != 0);
    unsigned long now = fw.getEpoch();

    if(docked) {
      if(state != DOCKED) {
        notify("I'm now docked");
        state = DOCKED;
      }
      lastDockedTime = now;
    }
    if(moving) {
      if(state != CLEANING) {
        notify("I'm now cleaning");
        state = CLEANING;
      }
      lastCleanTime = now;
    }
    // If we are not charging and not moving we are probably stuck.
    if(!docked && !moving && (now - lastCleanTime > 10000)) {
      if(state != STALLED) {
        notify("I'm stuck, please help !");
        state = STALLED;
      }
      lastStallTime = fw.getEpoch();
    }
    pollCount++;
  }
  void run() {
    if(Serial.available()) {
      Serial.read();
      Serial.println("Ignoring input");
  }
    if(millis() - lastPoll > 1000) { // Poll every second
      doPoll();
      lastPoll = millis();
  }
  notifier.run();
  //yield();
  //asm("waiti 0");
  //yield();
}
static constexpr Cmds cmds[20] = {
    {"wakeup",  0, {}          ,   NULL},
    {"send",    0, {}          ,   &MyApp::handleSend},
    {"control", 2, { 128, 130 },   NULL},
    {"safe",    2, { 128, 131 },   NULL},
    {"full",    2, { 128, 132 },   NULL},
    {"power",   2, { 128, 133 },   NULL},
    {"spot",    2, { 128, 134 },   NULL},
    {"clean",   2, { 128, 135 },   NULL},
    {"max",     2, { 128, 136 },   NULL},
    {"drive",   2, { 128, 137 },   NULL},
    {"motors",  2, { 128, 138 },   NULL},
    {"leds",    2, { 128, 139 },   NULL},
    {"song",    2, { 128, 140 },   NULL},
    {"play",    2, { 128, 141 },   NULL},
    {"sensors", 0, {}, &MyApp::handleSensors},
    {"stats",   0, {}, &MyApp::handleStats},
    {"testmsg", 0, {}, &MyApp::handleTestMsg},
    {"dock",    2, { 128, 143 },   NULL},
#if 0
    {"beep",   10, { 128, 132 , WAIT, 140, 0, 1, 62, 32, WAIT, 141, 0 }, NULL},
#endif
    {NULL,0,{},NULL}
};
};
constexpr MyApp::Cmds MyApp::cmds[20];
MyApp myApp;

void setup(void) {
    fw.begin();
  myApp.begin();
}
void loop(void) {
  fw.run();
  myApp.run();
}
