#include "HTTPFileServer.h"
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>

void HTTPFileServer::setup(ESP8266WebServer* server){
  SPIFFS.begin();
  server->on("/format", [this,server]() {
    SPIFFS.format();
    server->send(200,"text/plain", "FORMATTED");
  });
  server->on("/upload", HTTP_POST,
    [this, server]() {
      server->send(200, "text/plain", "");
    },
    [this, server]() {
      HTTPUpload& upload = server->upload();
      if(upload.status == UPLOAD_FILE_START){
        String filename = upload.filename;
        if(!filename.startsWith("/")) filename = "/"+filename;
        fsUploadFile = SPIFFS.open(filename, "w");
        filename = String();
      } else if(upload.status == UPLOAD_FILE_WRITE){
        if(fsUploadFile)
        fsUploadFile.write(upload.buf, upload.currentSize);
      } else if(upload.status == UPLOAD_FILE_END){
        if(fsUploadFile)
        fsUploadFile.close();
      }
    }
  );
  server->onNotFound([this, server]() {
    String path = server->uri();
    if(path.endsWith("/"))
    path += "sv.html";
    String contentType = getContentType(server->hasArg("download"), path);
    if(SPIFFS.exists(path)) {
      File file = SPIFFS.open(path, "r");
      size_t sent = server->streamFile(file, contentType);
      file.close();
    }
    else
      server->send(404, "text/plain", "FileNotFound");
  });
}
String HTTPFileServer::getContentType(bool download, String filename) {
  if(download) return "application/octet-stream";
  else if(filename.endsWith(".htm")) return "text/html";
  else if(filename.endsWith(".html")) return "text/html";
  else if(filename.endsWith(".csv")) return "text/csv";
  else if(filename.endsWith(".css")) return "text/css";
  else if(filename.endsWith(".js")) return "application/javascript";
  else if(filename.endsWith(".png")) return "image/png";
  else if(filename.endsWith(".gif")) return "image/gif";
  else if(filename.endsWith(".jpg")) return "image/jpeg";
  else if(filename.endsWith(".ico")) return "image/x-icon";
  else if(filename.endsWith(".xml")) return "text/xml";
  else if(filename.endsWith(".pdf")) return "application/x-pdf";
  else if(filename.endsWith(".zip")) return "application/x-zip";
  else if(filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}
