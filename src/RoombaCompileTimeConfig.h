#ifndef _ROOMBA_COMPILE_TIME_CONFIG_H_
#define _ROOMBA_COMPILE_TIME_CONFIG_H_
// Compile time configuration
//
// FIXME: All of this should be runtime configurable
//
#include "Slack.h"

class Config {
public:
	// Get this token here: https://api.slack.com/web
	static const char* slackToken;
	// Sender name user (but actually sent from slackbot)
	static const char* slackSender;
	// The slack channel name or @your-name
	static const char* slackChannel;
};

const char* Config::slackToken = "xoxp-10726680343-10730973185-13625061332-0e08ede108";
const char* Config::slackSender = "Roomba";
const char* Config::slackChannel = "@frma";

class Notifier : public Slack {};

#endif // _ROOMBA_COMPILE_TIME_CONFIG_H_