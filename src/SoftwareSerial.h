#include <Esp.h>

class SoftwareSerial : public Stream {
 public:
  SoftwareSerial(int baud, int txPin);
  void begin();
  size_t write(uint8_t a);
  int available() { return 0;}
  int read() { return 0;}
  int peek() { return 0;}
  void flush() {}  
 private:
  static const int tickPerUS = 80;
  static const int scale = 16;
  unsigned int bitTime;
  int txPin;

  inline uint32_t now()
  {
    uint32_t ccount;
    __asm__ __volatile__("esync; rsr %0,ccount":"=a" (ccount));
    return ccount;
  }

  inline void wait_until(unsigned int start, int bit) {
    unsigned int offset = (bit*bitTime)/scale;
    while(now() - start < offset);
  }
};
