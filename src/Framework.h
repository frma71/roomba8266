#include <WiFiServer.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include "HTTPFileServer.h"
#include "NtpTime.h"
#include "app.h"

/* PROGMEM
 */
extern const char mime_text[];
extern const char mime_html[];

class Framework {
public:
	Framework(const char* appName);
	void savePrefs();
	void begin();
	void run();
	unsigned long getEpoch(){
		return ntp.getEpoch();
	}

	struct Prefs {
	  int version;
	  char ssid[32];
	  char passwd[64];
	  struct AppPrefs app;
	};
	const char* appName;
	Stream& Debug;
	ESP8266WebServer webServer;
	HTTPFileServer fileServer;
	ESP8266HTTPUpdateServer httpUpdater;
	struct Prefs prefs;
	NtpTime ntp;
};
